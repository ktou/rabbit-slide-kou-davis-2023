= Apache Arrow, Red Arrow, Red Data Tools

Apache Arrow、Red Arrow、Red Data Toolsという観点からRubyにおけるデータ処理の現状を説明します。

== ライセンス

=== スライド

CC BY-SA 4.0

原著作者名は以下の通りです。

  * 須藤功平（またはSutou Kouhei）

=== 画像

==== クリアコードのロゴ

CC BY-SA 4.0

原著作者：株式会社クリアコード

ページヘッダーで使っています。

== 作者向け

=== 表示

  rake

=== 公開

  rake publish

== 閲覧者向け

=== インストール

  gem install rabbit-slide-kou-davis-2023

=== 表示

  rabbit rabbit-slide-kou-davis-2023.gem

